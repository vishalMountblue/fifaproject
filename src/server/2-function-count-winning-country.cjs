
const winningCountries = (matches) =>{   
    //console.log( matches)


    let winningTeams = matches.map( (match) =>{
        console.log(match["Home Team Name"])

        if(match["Home Team Name"].length !== 0 && match["Away Team Name"].length !== 0 ){


            let homeTeamGoalCount = match["Home Team Goals"]
            let awayTeamGoalCount = match["Away Team Goals"]

            if(awayTeamGoalCount > homeTeamGoalCount){

                return match["Away Team Name"]

            }else if (homeTeamGoalCount > awayTeamGoalCount){

                return match["Home Team Name"]
            }
        }
    })

    

    let teamsCount = winningTeams.reduce( (accumulator,currentValue) =>{

        if(accumulator[currentValue]){

            accumulator[currentValue] += 1

        }else{

            accumulator[currentValue] = 1
        }

        return accumulator

    },{})

    return teamsCount
    

}

module.exports = winningCountries