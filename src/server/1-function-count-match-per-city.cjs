
const countingMatchesFunction = (matches) => {

  let cities = fetchCities(matches)
  let cityMatcheCountObject = {}

  cities.map( (city) =>{

    if(cityMatcheCountObject[city]){

      cityMatcheCountObject[city]++;
    }else{
      cityMatcheCountObject[city] = 1
    }

  })

  return cityMatcheCountObject

};

const fetchCities = (matches) =>{

  
  let filteredCities = matches.filter((match) =>{

    
    if(match.City.length !==0){
      return match.City
    }
  })

  let cities = filteredCities.map((match) => match.City )

  return cities

}




module.exports = countingMatchesFunction;
