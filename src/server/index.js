
const worldCupMatchesCsvFile='../data/WorldCupMatches.csv'
const worldCupsCsv = '../data/WorldCups.csv'
const worldCupPlayersCsvFile = "../data/WorldCupPlayers.csv";

const csv=require('csvtojson')
const fs = require('fs')
const prompt = require('prompt-sync')()

const countingMatchesFunction = require('./1-function-count-match-per-city.cjs')
const winningCountries = require('./2-function-count-winning-country.cjs')
const redCardProcessingFunction = require('./3-function-process-red-cards')
const probablityFunction = require('./4-function-process-probablity.cjs')


// 1st problem - number of matches per city

csv()
.fromFile(worldCupMatchesCsvFile)
.then((dataset)=>{

    matchPerCity(dataset)
     
})

const matchPerCity = (dataset) =>{


    const cityMatchesCount = JSON.stringify(countingMatchesFunction(dataset))
    

    const outputFilePath = '../public/output/1-result.json'
    writeOutput(outputFilePath,cityMatchesCount)
}


// 2nd question - matches won per team


csv()
.fromFile(worldCupMatchesCsvFile)
.then((dataset)=>{

    numberOfMatchesWon(dataset)

})


const numberOfMatchesWon = (dataset) =>{

    const winningMatchCountResult = JSON.stringify(winningCountries(dataset))
    const outputFilePath = '../public/output/2-matches-won.json'

    writeOutput(outputFilePath,winningMatchCountResult) 
}



// 3rd question - calculating number of red cards issued


csv()
  .fromFile(worldCupPlayersCsvFile)
  .then((worldCupPlayerData) => {

    processRedCard(worldCupPlayerData);
  });


const processRedCard = (worldCupPlayerData) => {

  csv()
    .fromFile(worldCupMatchesCsvFile)
    .then((worldCupMatchesData) => {
     
   
    let year = prompt('Enter year: ')
      console.log(year)
      
      const result = JSON.stringify(
        redCardProcessingFunction(worldCupPlayerData,worldCupMatchesData, year)
      );

      const outputFilePath = "../public/output/3-red-card.json"

      writeOutput(outputFilePath,result)

    });
};
 


// 4th question - top players probablity

csv()
.fromFile(worldCupPlayersCsvFile)
.then((playerDataset) =>{

    const result = JSON.stringify(
        probablityFunction(playerDataset)
      );

      const outputFilePath = "../public/output/4-top-players-probablity.json"

      writeOutput(outputFilePath,result)

})

const writeOutput = (jsonPath,result) =>{

    fs.writeFile(jsonPath, result, (error) => {
        if (error) {
          console.log(error);
        }
      });

}
