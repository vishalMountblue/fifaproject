
const calculatesProbablity = (playerDataset) => {
  let regex = /G/g;
  
  let nameToPlayerDataObject = {};
  let matchCount = 1;

  let goalObject = playerDataset.map((player) => {

    let playerEvent = player["Event"];
    let goalCount = 0;
    let probablity = 0
    

    if (playerEvent.includes("G")) {
      goalCount = playerEvent.match(regex).length;
    } 

    if (nameToPlayerDataObject[player["Player Name"]]) {
      let oldPlayerData = nameToPlayerDataObject[player["Player Name"]];

      nameToPlayerDataObject[player["Player Name"]].goalCount = oldPlayerData.goalCount + goalCount;
      nameToPlayerDataObject[player["Player Name"]].matchCount = oldPlayerData.matchCount+1;
      nameToPlayerDataObject[player["Player Name"]].probablity = (nameToPlayerDataObject[player["Player Name"]].goalCount/nameToPlayerDataObject[player["Player Name"]].matchCount)

    } else {
      nameToPlayerDataObject[player["Player Name"]] = { name: player["Player Name"],goalCount, matchCount ,probablity};
    }

    


    
  });

  let topPlayers = Object.values(nameToPlayerDataObject).sort((a,b) => b.probablity - a.probablity).slice(0,10)
    console.log(topPlayers);

    return topPlayers
  
};

module.exports = calculatesProbablity;
